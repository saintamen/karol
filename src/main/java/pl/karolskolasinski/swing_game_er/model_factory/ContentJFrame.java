package pl.karolskolasinski.swing_game_er.model_factory;

import pl.karolskolasinski.swing_game_er.model_factory.panels.LeftPanel;
import pl.karolskolasinski.swing_game_er.model_factory.panels.ResetPanel;
import pl.karolskolasinski.swing_game_er.model_factory.panels.RightPanel;

import javax.swing.*;
import java.awt.*;

public class ContentJFrame extends JFrame {
    private final static int WINDOW_WIDTH = 500, WINDOW_HEIGHT = 500;
    private final static String TITLE = "Get a secret code to open the safe.";

    public ContentJFrame() {
        /*jFrame size&location settings (size first!)*/
        this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        this.setLocation((int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2 - this.getWidth() / 2), 100);

        /*jFrame title*/

        /*jFrame behavior settings*/

        /*jFrame visual settings*/
//        this.setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getClassLoader().getResource("icons8-safe-16.png")));

        /*setVisible (last!)*/
        this.getContentPane().setLayout(null);
        this.getContentPane().add(new LeftPanel(WINDOW_HEIGHT));
        this.getContentPane().add(new ResetPanel(WINDOW_WIDTH));
        this.getContentPane().add(new RightPanel(WINDOW_WIDTH, WINDOW_HEIGHT));

        this.setTitle(TITLE);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setResizable(false);

        this.setVisible(true);
    }

    public JFrame getjFrame() {
        return this;
    }
}
