package pl.karolskolasinski.swing_game_er.model_factory.interfaces;

import pl.karolskolasinski.swing_game_er.model_factory.panels.ArrowButton;

public interface IPlayPanel {

    void moveDown(ArrowButton arrowButton);

    void moveUp(ArrowButton arrowButton);

}
