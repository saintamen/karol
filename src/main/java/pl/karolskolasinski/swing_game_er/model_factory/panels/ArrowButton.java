package pl.karolskolasinski.swing_game_er.model_factory.panels;

import pl.karolskolasinski.swing_game_er.model_factory.ButtonType;
import pl.karolskolasinski.swing_game_er.model_factory.interfaces.IPlayPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ArrowButton extends JButton implements ActionListener {
    private static final String DOWN_TEXT = "DOWN";//"\uD83D\uDD3B";
    private static final String UP_TEXT = "UP";//"\uD83D\uDD3A";


    private static final Color DOWN_COLOR = Color.RED;
    private static final Color UP_COLOR = Color.GREEN;

    private IPlayPanel playPanel;
    private ButtonType buttonType;

    public ButtonType getButtonType() {
        return buttonType;
    }

    public ArrowButton(IPlayPanel playPanel, ButtonType buttonType) {
        this.playPanel = playPanel;
        this.buttonType = buttonType;

        if (buttonType == ButtonType.DOWN) {
            setText(DOWN_TEXT);
            setForeground(DOWN_COLOR);
        } else if (buttonType == ButtonType.UP) {
            setText(UP_TEXT);
            setForeground(UP_COLOR);
        }

        setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));
        setFocusable(false);
        setRolloverEnabled(false);
        addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (buttonType == ButtonType.DOWN) {
            playPanel.moveDown(this);
//            GameStatusChecker.checkGameStatus(arrowJButtons, hexCodeJLabel);
        } else if (buttonType == ButtonType.UP) {
            playPanel.moveUp(this);
//            GameStatusChecker.checkGameStatus(arrowJButtons, hexCodeJLabel);
        }
    }

}
