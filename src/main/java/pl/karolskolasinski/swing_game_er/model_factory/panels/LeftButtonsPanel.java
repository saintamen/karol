package pl.karolskolasinski.swing_game_er.model_factory.panels;

import pl.karolskolasinski.swing_game_er.model_factory.ButtonFacory;
import pl.karolskolasinski.swing_game_er.model_factory.ButtonType;
import pl.karolskolasinski.swing_game_er.model_factory.interfaces.IPlayPanel;

import javax.swing.*;
import java.awt.*;

public class LeftButtonsPanel extends JPanel implements IPlayPanel {
    private ArrowButton[] arrowButtons = new ArrowButton[7];

    public LeftButtonsPanel() {
        setLayout(new GridLayout(7, 1));
//        removeAll();

        populatebuttons();

        addAll();
    }

    private void addAll() {
        removeAll();
        for (int i = 0; i < arrowButtons.length; i++) {
            if (arrowButtons[i] != null) {
                add(arrowButtons[i]);
            }
        }
    }

    private void populatebuttons() {
        arrowButtons[0] = createDownButton();
        arrowButtons[1] = createDownButton();
        arrowButtons[2] = createDownButton();
        arrowButtons[3] = createEmptyButton(); // empty field
        arrowButtons[4] = createUpButton();
        arrowButtons[5] = createUpButton();
        arrowButtons[6] = createUpButton();
    }

    private ArrowButton createEmptyButton() {
        return new ArrowButton(this, ButtonType.NONE);
    }

    private ArrowButton createDownButton() {
        return new ArrowButton(this, ButtonType.DOWN);
    }

    private ArrowButton createUpButton() {
        return new ArrowButton(this, ButtonType.UP);
    }

    @Override
    public void moveDown(ArrowButton arrowButton) {
        int position = findPosition(arrowButton);
        try {
            if (arrowButtons[position + 1].getButtonType() == ButtonType.NONE) {
                ArrowButton tmp = arrowButtons[position];
                ArrowButton changed = arrowButtons[position + 1];
                arrowButtons[position] = changed;
                arrowButtons[position + 1] = tmp;

            } else if (arrowButtons[position + 1].getButtonType() == ButtonType.UP
                    && arrowButtons[position + 2].getButtonType() == ButtonType.NONE) {
                ArrowButton tmp = arrowButtons[position];
                ArrowButton changed = arrowButtons[position + 2];
                arrowButtons[position] = changed;
                arrowButtons[position + 2] = tmp;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println(e.getMessage());
        }

        addAll();
        revalidate();
        repaint();
    }


    private int findPosition(ArrowButton arrowButton) {
        for (int i = 0; i < arrowButtons.length; i++) {
            if (arrowButtons[i] == arrowButton) {
                return i;
            }
        }
        throw new UnsupportedOperationException("");
    }

    @Override
    public void moveUp(ArrowButton arrowButton) {
        int position = findPosition(arrowButton);
        try {
            if (arrowButtons[position - 1].getButtonType() == ButtonType.NONE) {
                ArrowButton tmp = arrowButtons[position];
                ArrowButton changed = arrowButtons[position - 1];
                arrowButtons[position] = changed;
                arrowButtons[position - 1] = tmp;

            } else if (arrowButtons[position - 1].getButtonType() == ButtonType.DOWN
                    && arrowButtons[position - 2].getButtonType() == ButtonType.NONE) {
                ArrowButton tmp = arrowButtons[position];
                ArrowButton changed = arrowButtons[position - 2];
                arrowButtons[position] = changed;
                arrowButtons[position - 2] = tmp;

            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println(e.getMessage());
        }

        addAll();
        revalidate();
        repaint();
    }
}
