package pl.karolskolasinski.swing_game_er.model_factory.panels;

import pl.karolskolasinski.swing_game_er.model_factory.ButtonFacory;
import pl.karolskolasinski.swing_game_er.model_factory.ButtonType;
import pl.karolskolasinski.swing_game_er.model_factory.interfaces.IPlayPanel;

import javax.swing.*;

public class LeftPanel extends JPanel {
    private final static String PANEL_TITLE = "Play this game";
    protected final static int PANEL_WIDTH = 250;

    private LeftButtonsPanel buttonsPanel;

    public LeftPanel(int windowHeight) {
        setBounds(0, 0, PANEL_WIDTH, windowHeight);
        setBorder(BorderFactory.createTitledBorder(PANEL_TITLE));

        createInsidePanel();
    }

    private void createInsidePanel() {
        add(new LeftButtonsPanel());
    }

}
