package pl.karolskolasinski.swing_game_er.model_factory.panels;

import javax.swing.*;

public class ResetPanel extends JPanel {
    private final static String PANEL_TITLE = "Reset";
    protected final static int PANEL_HEIGHT = 120;

    public ResetPanel(int windowWidth) {
        setBounds(LeftPanel.PANEL_WIDTH, 0, windowWidth - LeftPanel.PANEL_WIDTH, PANEL_HEIGHT);
        setBorder(BorderFactory.createTitledBorder(PANEL_TITLE));

    }
}
