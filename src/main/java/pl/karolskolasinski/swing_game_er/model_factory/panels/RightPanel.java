package pl.karolskolasinski.swing_game_er.model_factory.panels;

import javax.swing.*;

public class RightPanel extends JPanel {
    private final static String PANEL_TITLE = "Get the code";

    public RightPanel(int windowWidth, int windowHeight) {
        setBounds(LeftPanel.PANEL_WIDTH,
                ResetPanel.PANEL_HEIGHT,
                windowWidth - LeftPanel.PANEL_WIDTH,
                windowHeight - ResetPanel.PANEL_HEIGHT);
        setBorder(BorderFactory.createTitledBorder(PANEL_TITLE));

    }
}
